<?php
/**
 * @author: StefanHelmer
 */

namespace Rockschtar\GoAOPAspects\Aspect;

use Go\Aop\Aspect;
use Go\Lang\Annotation\After;
use Go\Lang\Annotation\Before;
use Rockschtar\GoAOPAspects\Exceptions\NoticeException;
use Rockschtar\GoAOPAspects\Exceptions\WarningException;

class ErrorHandlerAspect implements Aspect {
    /**
     * Method that will be called before real method
     *
     * @Before("@execution(Rockschtar\GoAOPAspects\Annotations\ThrowExceptionForEverything)")
     */
    public function setErrorHandler(): void {
        set_error_handler(function ($errno, $errstr, $errfile, $errline) {
            if (0 === error_reporting()) {
                return false;
            }

            switch($errno) {
                case E_NOTICE:
                    throw new NoticeException($errstr, 0, $errno, $errfile, $errline);
                case E_WARNING:
                    throw new WarningException($errstr, 0, $errno, $errfile, $errline);
                default:
                    throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);

            }
        });
    }

    /**
     * Method that will be called after real method
     *
     * @After("@execution(Rockschtar\GoAOPAspects\Annotations\ThrowExceptionForEverything)")
     */
    public function restoreErrorHandler(): void {
        restore_error_handler();
    }
}