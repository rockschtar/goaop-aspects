<?php
/**
 * @author: StefanHelmer
 */

namespace Rockschtar\GoAOPAspects\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
class ThrowExceptionForEverything extends Annotation {

}